FROM fedora:41 AS base

RUN dnf update -y --refresh && dnf install -y git cmake gcc g++ grpc protobuf openssl-devel \
        protobuf-devel grpc-devel grpc-cpp grpc-plugins libuuid-devel glog-devel fuse3-devel \
        fuse3-libs tomlplusplus-devel libseccomp-devel \
        && dnf clean all \
        && rm -rf /var/cache/yum

RUN mkdir /src/

WORKDIR /src

RUN git clone https://gitlab.com/BuildGrid/buildbox/buildbox.git

WORKDIR /src/buildbox

RUN git fetch && git checkout 1.2.24

RUN mkdir build && cd build && cmake .. && make && make install

FROM fedora:41 AS ci-base

RUN dnf update -y --refresh && dnf install -y git git-lfs cmake fuse3-devel \
        fuse3-libs gcc g++ glog grpc grpc-cpp xz bubblewrap procps-ng openssl-devel libseccomp-devel \
        podman qemu-system-aarch64 qemu-system-x86 \
        && dnf clean all \
        && rm -rf /var/cache/yum

# Get Rust
RUN curl https://sh.rustup.rs -sSf | bash -s -- -y

RUN echo 'source $HOME/.cargo/env' >> $HOME/.bashrc

ENV PATH="/root/.cargo/bin:${PATH}"

RUN cargo install mdbook cargo-tarpaulin cargo-nextest

RUN rustup component add clippy

FROM ci-base AS ci

COPY --from=base /usr/local/bin/* ./usr/bin/

RUN ln /usr/bin/buildbox-run-bubblewrap /usr/bin/buildbox-run

WORKDIR /src

RUN git clone https://gitlab.com/girderstream/girderstream

WORKDIR /src/girderstream

RUN git fetch && git checkout 322feca067357ff579264a58d4f0e373874046a9

RUN cargo build && cargo build --release

RUN cargo install --path crates/girderstream  --locked --force

RUN cargo clippy --no-deps --all-targets --all-features -- -D warnings

RUN cargo doc --document-private-items

FROM fedora:41 AS target

RUN dnf update -y --refresh && dnf install -y fuse3-devel \
        fuse3-libs glog grpc grpc-cpp xz bubblewrap libseccomp-devel \
        && dnf clean all \
        && rm -rf /var/cache/yum

COPY --from=base /usr/local/bin/* ./usr/bin/

RUN ln /usr/bin/buildbox-run-bubblewrap /usr/bin/buildbox-run

COPY --from=ci /root/.cargo/bin/girderstream* ./usr/bin/

FROM fedora:41 AS yaba

RUN dnf update -y --refresh && dnf install -y git git-lfs cmake fuse3-devel \
        gcc fuse3-libs xz grpc grpc-cpp procps-ng openssl-devel libseccomp-devel \
        && dnf clean all \
        && rm -rf /var/cache/yum

# Get Rust
RUN curl https://sh.rustup.rs -sSf | bash -s -- -y

RUN echo 'source $HOME/.cargo/env' >> $HOME/.bashrc

ENV PATH="/root/.cargo/bin:${PATH}"

RUN cargo install mdbook cargo-tarpaulin

RUN rustup component add clippy

WORKDIR /src

RUN git clone https://gitlab.com/girderstream/yet-another-build-app

WORKDIR /src/yet-another-build-app

RUN git fetch && git checkout e90422e4cc2dbaa2c444c3dacf1987d2b3529d2b

RUN cargo build && cargo build --release

RUN cargo install --path .  --locked --force

FROM fedora:41 AS target-yaba

RUN dnf update -y --refresh && dnf install -y fuse3-devel \
        fuse3-libs xz libseccomp-devel \
        && dnf clean all \
        && rm -rf /var/cache/yum

COPY --from=ci /root/.cargo/bin/girderstream* ./usr/bin/
COPY --from=yaba /root/.cargo/bin/yaba* ./usr/bin/
