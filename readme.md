# Create Girderstream docker images

This repo has docker files for use with girderstream and its ci creates images
for them.

These can then be used by the Girderstream projects ci or others.

You can build them yourself as follows.

```
docker build . --target ci  -t tag_to_test
```

```
docker run --privileged tag_to_test cargo test --tests
```

Or pull them from the gitlab registry.

```
docker pull registry.gitlab.com/girderstream/docker-images/girderstream-ci:latest
```

```
docker run --privileged registry.gitlab.com/girderstream/docker-images/girderstream-ci:latest cargo test --tests
```
